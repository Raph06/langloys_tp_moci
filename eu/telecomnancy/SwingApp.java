package eu.telecomnancy;

import eu.telecomnancy.sensor.AbSensor;
import eu.telecomnancy.sensor.Adapter;
import eu.telecomnancy.sensor.ISensor;
import eu.telecomnancy.State.SensorState;
import eu.telecomnancy.sensor.TemperatureSensor;
import eu.telecomnancy.ui.MainWindow;

public class SwingApp {

    public static void main(String[] args) {
        
        ISensor sensor = new TemperatureSensor();
        ISensor adapter = new Adapter();
        ISensor state = new SensorState();
        
        new MainWindow(sensor);
    }
}