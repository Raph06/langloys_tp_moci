/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.telecomnancy.sensor;

import eu.telecomnancy.ui.SensorView;
import java.text.DateFormat;
import java.util.Date;

/**
 *
 * @author Raph
 */
public class SensorProxy extends AbSensor{
    
	static Date date;
	DateFormat mediumDateFormat = DateFormat.getDateTimeInstance(DateFormat.MEDIUM, DateFormat.MEDIUM);

    private ISensor sensor;
    private SensorLogger log;

    public SensorProxy(ISensor _sensor, SensorLogger sensorLogger) {
        sensor = sensor;
        log = sensorLogger;
    }

 
    public void on() {
        log.log(LogLevel.INFO, "Sensor On");
        sensor.on();
        date = new Date();
        System.out.println(mediumDateFormat.format(date) + " : méthode on() : " + sensor.getStatus());

    }

 
    public void off() {
        log.log(LogLevel.INFO, "Sensor Off");
        sensor.off();
        date = new Date();
        System.out.println(mediumDateFormat.format(date) + " : méthode off() : " + sensor.getStatus());

    }
  
    public void update() throws SensorNotActivatedException {
    	date = new Date();
        System.out.println(mediumDateFormat.format(date) + " : méthode update() : " + sensor.getValue());
        log.log(LogLevel.INFO, "Sensor update");
        sensor.update();
    }


    public boolean getStatus() {
    	date = new Date();
        System.out.println(mediumDateFormat.format(date) + " : méthode getStatus() : " + sensor.getStatus());
        log.log(LogLevel.INFO, "Sensor getStatus");
        return sensor.getStatus();
    }

 
 
    public double getValue() throws SensorNotActivatedException {
    	date = new Date();
        System.out.println(mediumDateFormat.format(date) + " : méthode update() : " + sensor.getValue());
        log.log(LogLevel.INFO, "Sensor value =" + sensor.getValue());
        return sensor.getValue();
    }

}
