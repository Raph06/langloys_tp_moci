/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.telecomnancy.sensor;

import eu.telecomnancy.Observer.Subject;
import eu.telecomnancy.Observer.Observer;
import java.util.ArrayList;

/**
 *
 * @author Raph
 */
public abstract class AbSensor implements ISensor,Subject {
    
     private ArrayList<Observer> listObserver = new ArrayList<Observer>();
	
	
		public void addObserver(Observer o) {

			listObserver.add(o);
		}

		
		public void removeObserver(Observer o) {
			
			listObserver.remove(o);
		}

		public void notifyObserver() throws SensorNotActivatedException {
			
			for(int i=0;i<listObserver.size();i++)
			{
				Observer o = listObserver.get(i);
				o.update();
			}
		}
    
}
