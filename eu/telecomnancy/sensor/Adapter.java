/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.telecomnancy.sensor;

/**
 *
 * @author Raph
 */
public class Adapter extends AbSensor {
    
    private LegacyTemperatureSensor lTS = new LegacyTemperatureSensor();
    
    public void on() {
        if (!lTS.getStatus()){
            lTS.onOff();
        }
    }

    @Override
    public void off() {
       if (lTS.getStatus()){
            lTS.onOff();
        }
    }

    @Override
    public boolean getStatus() {
        return lTS.getStatus();
    }
    @Override
    public void update() throws SensorNotActivatedException {
        lTS.onOff();
        lTS.onOff();
        notifyObserver();
    }

    @Override
    public double getValue() {
        return lTS.getTemperature();
    }


}
