/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.telecomnancy.Observer;

import eu.telecomnancy.Observer.Observer;
import eu.telecomnancy.sensor.SensorNotActivatedException;



/**
 *
 * @author Raph
 */
public interface Subject {
    
   
    
    public void addObserver(Observer o);
    
    public void removeObserver(Observer o);
    
    public void notifyObserver() throws SensorNotActivatedException;
}
    
    

