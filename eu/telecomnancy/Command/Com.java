/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.telecomnancy.Command;

import eu.telecomnancy.sensor.Adapter;
import eu.telecomnancy.Decorator.Approx;
import eu.telecomnancy.Decorator.ConvFahrenheit;
import eu.telecomnancy.sensor.ISensor;
import eu.telecomnancy.ui.ConsoleUI;

/**
 *
 * @author Raph
 */
public class Com {
    
    public static void main(String[] args) {
        ISensor sensor = new Approx(new ConvFahrenheit(new Adapter()));
        new ConsoleUI(sensor);
    }
    
}
