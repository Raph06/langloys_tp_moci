/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.telecomnancy.Command;

import eu.telecomnancy.Command.ICommand;

/**
 *
 * @author Raph
 */
public class Exec {
    
    ICommand cmd;
    
    public Exec(ICommand cmd){
        this.cmd=cmd;
    }
    
    public void launch(ICommand cmd){
        cmd.execute();
    }
    
}
