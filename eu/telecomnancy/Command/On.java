/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.telecomnancy.Command;

import eu.telecomnancy.Command.ICommand;
import eu.telecomnancy.sensor.TemperatureSensor;

/**
 *
 * @author Raph
 */
public class On implements ICommand {
    
    private TemperatureSensor ts;
    
    public On(TemperatureSensor ts){
        
        this.ts=ts;
    }
    
    public void execute(){
        
        ts.on();
    }
    
    
    
}
