/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.telecomnancy.State;

import eu.telecomnancy.State.SensorOff;
import eu.telecomnancy.State.SensorOn;
import eu.telecomnancy.State.IState;
import eu.telecomnancy.State.Context;
import eu.telecomnancy.sensor.AbSensor;
import eu.telecomnancy.sensor.SensorNotActivatedException;
import java.util.Random;

/**
 *
 * @author Raph
 */
public class SensorState extends AbSensor{
    
    private double value;
	
private Context context;
    
    public SensorState() {
       this.context = new Context();
    }

    @Override
    public void on() {
        IState on = new SensorOn();
        on.execute(context);
    }

    @Override
    public void off() {
        IState off = new SensorOff();
        off.execute(context);
        
    }

    @Override
    public boolean getStatus() {
        return context.getState() instanceof SensorOn;
     
    }

    @Override
    public void update() throws SensorNotActivatedException {
        if (getStatus()){
            value = (new Random()).nextDouble() * 100;
            notifyObserver();
        }
        else throw new SensorNotActivatedException("Sensor must be activated before acquiring new values");
    }

    @Override
    public double getValue() throws SensorNotActivatedException {
       if (getStatus())
	            return value;
         else throw new SensorNotActivatedException("Sensor must be activated to get its value.");
    }
    
   
}