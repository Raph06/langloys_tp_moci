/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.telecomnancy.Decorator;

import eu.telecomnancy.Decorator.Decorateur;
import eu.telecomnancy.sensor.ISensor;
import eu.telecomnancy.sensor.SensorNotActivatedException;

/**
 *
 * @author Raph
 */
public class Approx extends Decorateur{
    
    public Approx(ISensor sensor) {
	super(sensor);
    }
	
    public double getValue() throws SensorNotActivatedException {
	double a = Math.round(sensor.getValue()) ;
        return a;
    }
}
