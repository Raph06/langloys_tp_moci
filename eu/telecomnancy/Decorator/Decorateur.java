/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.telecomnancy.Decorator;

import eu.telecomnancy.sensor.AbSensor;
import eu.telecomnancy.sensor.ISensor;
import eu.telecomnancy.sensor.SensorNotActivatedException;

/**
 *
 * @author Raph
 */
public abstract class Decorateur extends AbSensor{
	
    protected ISensor sensor;
	
    public Decorateur(ISensor newSensor) {
	sensor = newSensor;
    }
	
    public double getValue() throws SensorNotActivatedException {
	return sensor.getValue();
    }
    
   public void on() {
	sensor.on();
    }

    public void off() {
	sensor.off();		
    }

    public boolean getStatus() {
	return sensor.getStatus();
    }

    public void update() throws SensorNotActivatedException {
        sensor.update();
		
    }    
    
}
