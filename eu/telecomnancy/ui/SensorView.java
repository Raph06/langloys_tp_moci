package eu.telecomnancy.ui;

import eu.telecomnancy.Decorator.Approx;
import eu.telecomnancy.Decorator.ConvFahrenheit;
import eu.telecomnancy.Decorator.Decorateur;
import eu.telecomnancy.sensor.ISensor;
import eu.telecomnancy.Observer.Observer;
import eu.telecomnancy.sensor.SensorNotActivatedException;
import java.awt.BorderLayout;
import java.awt.Font;
import java.awt.GridLayout;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;


public class SensorView extends JPanel implements Observer {
    private ISensor sensor;

    private JLabel value = new JLabel("N/A °C");
    private JButton on = new JButton("On");
    private JButton off = new JButton("Off");
    private JButton update = new JButton("Acquire");
    private JButton fahrenheit = new JButton("Fahrenheit");
    private JButton arrondir = new JButton("Round");
            
    public SensorView(ISensor c) {
        this.sensor = c;
        this.setLayout(new BorderLayout());

        value.setHorizontalAlignment(SwingConstants.CENTER);
        Font sensorValueFont = new Font("Sans Serif", Font.BOLD, 18);
        value.setFont(sensorValueFont);

        this.add(value, BorderLayout.CENTER);


        on.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                sensor.on();
            }
        });

        off.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                sensor.off();
            }
        });

        update.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    sensor.update();
                } catch (SensorNotActivatedException sensorNotActivatedException) {
                    sensorNotActivatedException.printStackTrace();
                }
            }
        });
        
        fahrenheit.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Decorateur fahr = new ConvFahrenheit(sensor);
                try {
                   String temp = String.valueOf(fahr.getValue());
                   value.setText(temp+"°F");
                } catch (SensorNotActivatedException ex) {
                    Logger.getLogger(SensorView.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        });
        
         arrondir.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Decorateur convert = new Approx(sensor);
                ISensor sensorTemp=sensor;
                sensor=convert;
                try {
                    update();
                } catch (SensorNotActivatedException ex) {
                    Logger.getLogger(SensorView.class.getName()).log(Level.SEVERE, null, ex);
                }
                sensor=sensorTemp;
            }
        });

        JPanel buttonsPanel = new JPanel();
        buttonsPanel.setLayout(new GridLayout(1, 3));
        buttonsPanel.add(update);
        buttonsPanel.add(on);
        buttonsPanel.add(off);
        buttonsPanel.add(fahrenheit);
        buttonsPanel.add(arrondir);
        
        

        this.add(buttonsPanel, BorderLayout.SOUTH);
        sensor.addObserver(this);
    }

    @Override
    public void update() throws SensorNotActivatedException{
        String tmp=String.valueOf(sensor.getValue());
         value.setText(tmp+"°C");
    }
}