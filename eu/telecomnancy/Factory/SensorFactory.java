/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.telecomnancy.Factory;

import eu.telecomnancy.helpers.ReadPropertyFile;
import eu.telecomnancy.sensor.ISensor;
import java.io.IOException;
import java.util.Properties;

/**
 *
 * @author Raph
 */
public abstract class SensorFactory {
    
    public static ISensor makeSensor() {
        ReadPropertyFile rp = new ReadPropertyFile();
        Properties p = null;
        try {
            p = rp.readFile("/eu/telecomnancy/app.properties");
        } catch (IOException e) {
            e.printStackTrace();
        }
        String factory = p.getProperty("factory");
        ISensor sensor;
        sensor = null;
        try {
            SensorFactory sensorfactory = (SensorFactory) Class.forName(factory).newInstance();
            sensor = sensorfactory.getSensor();

        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return sensor;
    }

    public abstract ISensor getSensor();
    
}
