/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.telecomnancy.Factory;

import eu.telecomnancy.sensor.ISensor;
import eu.telecomnancy.sensor.TemperatureSensor;

/**
 *
 * @author Raph
 */
public class SimpleSensorFactory {
    
    public ISensor getSensor() {
        return new TemperatureSensor();
    }
    
}
