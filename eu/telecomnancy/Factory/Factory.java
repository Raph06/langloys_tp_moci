/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.telecomnancy.Factory;

import eu.telecomnancy.helpers.ReadPropertyFile;
import eu.telecomnancy.sensor.Adapter;
import eu.telecomnancy.sensor.ISensor;
import eu.telecomnancy.sensor.TemperatureSensor;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Properties;

/**
 *
 * @author Raph
 */
public abstract class Factory extends SensorFactory {
    
    ArrayList<ISensor> list = new ArrayList<ISensor>();
    
    public Factory() throws IOException{
        
        ReadPropertyFile rp = new ReadPropertyFile();
        Properties p = rp.readFile("/eu/telecomnancy/sensor.properties");
        for (String i: p.stringPropertyNames()) {
            if(p.getProperty(i).equals("TemperatureSensor")){
                this.list.add(new TemperatureSensor());
            } else if(p.getProperty(i).equals("LegacyTemperatureSensor")){
                this.list.add(new Adapter());
            } else{
                System.out.println("erreur ligne "+i);
            }
        }
    }
    
    public ISensor getSensor() {
        return this.list.get(0);
    }
    
}
